module gitlab.com/Ivanchgeek/memarium/back

go 1.18

require (
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.5.0
)

require github.com/rs/cors v1.8.2

package game

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
	"time"
)

var Config Configuration

type Configuration struct {
	States struct {
		Opened         string `json:"opened"`
		Choosing       string `json:"choosing"`
		Answering      string `json:"answering"`
		SearchOriginal string `json:"search_original"`
		ShowOriginal   string `json:"show_original"`
		ShowScore      string `json:"show_score"`
		ShowWinner     string `json:"show_winner"`
	} `json:"states"`
	Values struct {
		MaxPlayers              int    `json:"max_players"`
		MinPlayers              int    `json:"min_players"`
		DefaultName             string `json:"default_name"`
		MemesCount              int    `json:"memes_count"`
		HandSize                int    `json:"hand_size"`
		OriginalShowTimeout_tmp int    `json:"original_show_timeout"`
		ScoreShowTimeout_tmp    int    `json:"score_show_timeout"`
		OriginalShowTimeout     time.Duration
		ScoreShowTimeout        time.Duration
		WinScore                int `json:"win_score"`
	} `json:"values"`
	MessageTypes struct {
		StateUpdate string `json:"state_update"`
		Init        string `json:"init"`
		Error       string `json:"error"`
		CMD         string `json:"cmd"`
	} `json:"message_types"`
}

func init() {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		panic("No caller information")
	}
	configFile, err := os.Open(path.Dir(filename) + "/../game-config.json")
	if err != nil {
		fmt.Println(err.Error())
	}
	defer configFile.Close()
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&Config)
	Config.Values.OriginalShowTimeout = time.Second * time.Duration(Config.Values.OriginalShowTimeout_tmp)
	Config.Values.ScoreShowTimeout = time.Second * time.Duration(Config.Values.ScoreShowTimeout_tmp)
	log.Println("current configuration: ")
	txt, _ := json.MarshalIndent(Config, "", "\t")
	log.Println(string(txt))
}

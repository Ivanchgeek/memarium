package game

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/Ivanchgeek/memarium/back/utl"
)

func (g *Game) ProccessCommand(from uuid.UUID, cmd map[string]interface{}) {
	switch data := g.data.(type) {
	case *OpenedStateData:
		g.stateOpenedHandler(from, cmd, data)
	case *ChoosingStateData:
		g.stateChoosingHandler(from, cmd, data)
	case *AnsweringStateData:
		g.stateAnsweringHandler(from, cmd, data)
	case *SearchOriginalStateData:
		g.stateSearchOriginalHandler(from, cmd, data)
	}
}

func (g *Game) stateOpenedHandler(from uuid.UUID, cmd map[string]interface{}, data *OpenedStateData) {
	switch cmd["cmd"].(string) {
	case "start":
		g.stateOpenedCmdStartHandler(from, data)
	}
}

func (g *Game) stateOpenedCmdStartHandler(from uuid.UUID, data *OpenedStateData) {
	if len(g.getPlayers()) < Config.Values.MinPlayers {
		g.sendMessage(from, Config.MessageTypes.Error, "need more players")
		return
	}
	if len(g.getPlayers()) > Config.Values.MaxPlayers {
		g.sendMessage(from, Config.MessageTypes.Error, "too much players")
		return
	}
	queue := make([]uuid.UUID, len(data.Players))
	i := 0
	for id := range data.Players {
		queue[i] = id
		i++
	}
	newState := ChoosingStateData{
		State:   ChoosingState,
		Players: data.Players,
		Queue:   queue,
		Chooser: queue[0],
	}
	for _, player := range newState.Players {
		player.Hand = make([]string, Config.Values.HandSize)
		for i := 0; i < Config.Values.HandSize; i++ {
			player.Hand[i] = g.pullFromDeck()
		}
		player.Score = 0
		player.Old_Score = 0
	}
	g.data = &newState
	g.updateStateRemote()
}

func (g *Game) stateChoosingHandler(from uuid.UUID, cmd map[string]interface{}, data *ChoosingStateData) {
	switch cmd["cmd"].(string) {
	case "choose":
		g.stateChoosingCmdChooseHandler(from, cmd, data)
	}
}

func (g *Game) stateChoosingCmdChooseHandler(from uuid.UUID, cmd map[string]interface{}, data *ChoosingStateData) {
	card, ok := cmd["card"].(string)
	if !(utl.ContainsCard(data.Players[data.Chooser].Hand, card) && ok) {
		g.sendMessage(from, Config.MessageTypes.Error, "no such card")
		return
	}
	description, ok := cmd["description"].(string)
	if !(len(description) > 0 && ok) {
		g.sendMessage(from, Config.MessageTypes.Error, "no description")
		return
	}
	newState := AnsweringStateData{
		State:       AnsweringState,
		Chooser:     data.Chooser,
		Players:     data.Players,
		Description: description,
		KeyImg:      card,
		Queue:       data.Queue,
	}
	newState.Players[newState.Chooser].Hand = utl.ReplaceCard(newState.Players[newState.Chooser].Hand, card, g.pullFromDeck())
	g.data = &newState
	g.updateStateRemote()
}

func (g *Game) stateAnsweringHandler(from uuid.UUID, cmd map[string]interface{}, data *AnsweringStateData) {
	switch cmd["cmd"].(string) {
	case "answer":
		g.stateAnsweringCmdAnswerHandler(from, cmd, data)
	}
}

func (g *Game) stateAnsweringCmdAnswerHandler(from uuid.UUID, cmd map[string]interface{}, data *AnsweringStateData) {
	card, ok := cmd["card"].(string)
	if !(utl.ContainsCard(data.Players[from].Hand, card) && ok) {
		g.sendMessage(from, Config.MessageTypes.Error, "no such card")
		return
	}

	newState := *data
	subject := *newState.Players[from]
	subject.Hand = utl.ReplaceCard(subject.Hand, card, g.pullFromDeck())
	subject.Answer = card
	newState.Players[from] = &subject

	everyBodyVoted := true
	for pid, player := range newState.Players {
		if len(player.Answer) == 0 && pid != newState.Chooser {
			everyBodyVoted = false
			break
		}
	}
	if everyBodyVoted {
		nextState := SearchOriginalStateData{
			State:       SearchOriginalState,
			Chooser:     newState.Chooser,
			Players:     newState.Players,
			Description: newState.Description,
			Queue:       newState.Queue,
		}
		nextState.Players[nextState.Chooser].Answer = newState.KeyImg
		g.data = &nextState
	} else {
		g.data = &newState
	}

	g.updateStateRemote()
}

func (g *Game) stateSearchOriginalHandler(from uuid.UUID, cmd map[string]interface{}, data *SearchOriginalStateData) {
	switch cmd["cmd"].(string) {
	case "assume":
		g.stateSearchOriginalCmdAssumeHandler(from, cmd, data)
	}
}

func (g *Game) stateSearchOriginalCmdAssumeHandler(from uuid.UUID, cmd map[string]interface{}, data *SearchOriginalStateData) {
	assume, ok := cmd["assume"].(string)
	if !ok {
		g.sendMessage(from, Config.MessageTypes.Error, "no assumption")
		return
	}

	newState := *data
	subject := *newState.Players[from]
	subject.OriginalAssumption = assume
	newState.Players[from] = &subject

	everyBodyAssumed := true
	for pid, player := range newState.Players {
		if len(player.OriginalAssumption) == 0 && pid != newState.Chooser {
			everyBodyAssumed = false
			break
		}
	}

	if everyBodyAssumed {
		nextState := ShowOriginalStateData{
			State:       ShowOriginalState,
			Chooser:     newState.Chooser,
			Players:     newState.Players,
			Description: newState.Description,
			Queue:       newState.Queue,
		}
		g.data = &nextState
		g.updateStateRemote()
		g.countScore()
		time.AfterFunc(Config.Values.OriginalShowTimeout, g.ProccessToShowScore)
	} else {
		g.data = &newState
	}
}

func (g *Game) countScore() {
	data, ok := g.data.(*ShowOriginalStateData)
	if !ok {
		return
	}
	correctAnswerCount := 0
	for pid, player := range data.Players {
		player.Old_Score = player.Score
		if pid == data.Chooser {
			continue
		}
		if player.OriginalAssumption == data.Players[data.Chooser].Answer {
			correctAnswerCount += 1
		}
	}

	switch correctAnswerCount {
	case len(data.Players) - 1:
		data.Players[data.Chooser].Score -= 3
	case 0:
		data.Players[data.Chooser].Score -= 2
	default:
		data.Players[data.Chooser].Score += 3
		for pid1, player1 := range data.Players {
			for pid2, player2 := range data.Players {
				if pid2 == pid1 || pid2 == data.Chooser {
					continue
				}
				if player2.OriginalAssumption == player1.Answer {
					player1.Score += 1
				}
			}
			if pid1 == data.Chooser {
				continue
			}
			if player1.OriginalAssumption == data.Players[data.Chooser].Answer {
				player1.Score += 3
			}
		}
	}

	for _, player := range data.Players {
		if player.Score < 0 {
			player.Score = 0
		}
	}
}

func (g *Game) ProccessToShowScore() {
	data, ok := g.data.(*ShowOriginalStateData)
	if !ok {
		return
	}
	newState := ShowScoreStateData{
		State:   ShowScoreState,
		Players: data.Players,
		Queue:   data.Queue,
	}
	g.data = &newState
	g.updateStateRemote()
	time.AfterFunc(Config.Values.ScoreShowTimeout, g.NextRound)
}

func (g *Game) NextRound() {
	data, ok := g.data.(*ShowScoreStateData)
	if !ok {
		return
	}
	for _, player := range data.Players {
		if player.Score >= Config.Values.WinScore {
			g.ShowWinner(player)
			return
		}
		player.Answer = ""
		player.OriginalAssumption = ""
	}
	queueUpdate := append(data.Queue, data.Queue[0])
	queueUpdate = queueUpdate[1:]
	newState := ChoosingStateData{
		State:   ChoosingState,
		Players: data.Players,
		Chooser: queueUpdate[0],
		Queue:   queueUpdate,
	}
	g.data = &newState
	g.updateStateRemote()
}

func (g *Game) ShowWinner(winner *Player) {
	g.data = &ShowWinnerStateData{
		State:   ShowWinnerState,
		Winner:  winner.Name,
		Players: g.getPlayers(),
	}
	g.updateStateRemote()
}

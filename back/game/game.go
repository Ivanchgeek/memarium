package game

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"

	"github.com/google/uuid"
)

type GameMessage struct {
	Target uuid.UUID   `json:"-"`
	Mtype  string      `json:"type"`
	Data   interface{} `json:"data"`
}

type Game struct {
	data         interface{}
	stateUpdater chan GameMessage
	deck         []string
}

func NewGame(code string, stateUpdater chan GameMessage) *Game {
	data := OpenedStateData{
		State:   OpenedState,
		Players: make(map[uuid.UUID]*Player),
		Code:    code,
	}
	result := Game{
		stateUpdater: stateUpdater,
		data:         &data,
		deck:         getDeck(),
	}
	return &result
}

func getDeck() []string {
	result := make([]string, Config.Values.MemesCount)
	for i := range result {
		result[i] = "meme" + strconv.Itoa(i) + ".jpg"
	}
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(result), func(i, j int) { result[i], result[j] = result[j], result[i] })
	return result
}

func (g *Game) pullFromDeck() string {
	result := g.deck[len(g.deck)-1]
	g.deck = g.deck[:len(g.deck)-1]
	return result
}

func (g *Game) getPlayers() map[uuid.UUID]*Player {
	switch data := g.data.(type) {
	case *OpenedStateData:
		return data.Players
	case *ChoosingStateData:
		return data.Players
	case *AnsweringStateData:
		return data.Players
	case *SearchOriginalStateData:
		return data.Players
	case *ShowOriginalStateData:
		return data.Players
	case *ShowScoreStateData:
		return data.Players
	case *ShowWinnerStateData:
		return data.Players
	}
	log.Println("can't get players for " + fmt.Sprintf("%v", g.data))
	return make(map[uuid.UUID]*Player)
}

func (g *Game) sendMessage(target uuid.UUID, mtype string, data interface{}) {
	if g.getPlayers()[target].Lost {
		return
	}
	g.stateUpdater <- GameMessage{
		Target: target,
		Mtype:  mtype,
		Data:   data,
	}
}

func (g *Game) broadcast(mtype string, data interface{}) {
	for id := range g.getPlayers() {
		g.sendMessage(id, mtype, data)
	}
}

func (g *Game) updateStateRemote() {
	g.broadcast(Config.MessageTypes.StateUpdate, g.data)
}

func (g *Game) IsAddingAllowed(name string) (bool, error) {
	if data, ok := g.data.(*OpenedStateData); ok {
		if name == "" {
			return true, nil
		}
		if len(data.Players) >= Config.Values.MaxPlayers {
			return false, errors.New("players limit")
		}
		for _, grabbedName := range data.Players {
			if grabbedName.Name == name {
				return false, errors.New("name garbbed")
			}
		}
	} else {
		return false, errors.New("game closed")
	}
	return true, nil
}

func (g *Game) AddPlayer(id uuid.UUID, name string, lobbyCode string) error {
	allowed, err := g.IsAddingAllowed(name)
	if !allowed {
		return err
	}
	data, _ := g.data.(*OpenedStateData)
	if name == "" {
		data.Players[id] = &Player{
			Name: Config.Values.DefaultName + "_" + id.String(),
			Lost: false,
		}
	} else {
		data.Players[id] = &Player{
			Name: name,
			Lost: false,
		}
	}
	g.data = data
	g.sendMessage(id, Config.MessageTypes.Init, struct {
		Id           uuid.UUID     `json:"id"`
		Config       Configuration `json:"config"`
		ReconnectKey string        `json:"reconnect_key"`
	}{
		Id:           id,
		Config:       Config,
		ReconnectKey: lobbyCode + "&" + id.String(),
	})
	g.updateStateRemote()
	return nil
}

func (g *Game) IsReconnectionAllowed(id uuid.UUID) bool {
	player := g.getPlayers()[id]
	if player == nil {
		return false
	}
	return player.Lost
}

func (g *Game) ReconnectPlayer(id uuid.UUID) {
	g.getPlayers()[id].Lost = false
	g.sendMessage(id, Config.MessageTypes.StateUpdate, g.data)
}

func (g *Game) DisconnectPlayer(id uuid.UUID) {
	switch data := g.data.(type) {
	case *OpenedStateData:
		delete(data.Players, id)
		g.data = data
		g.updateStateRemote()
	default:
		g.getPlayers()[id].Lost = true
	}
}

package game

import (
	"github.com/google/uuid"
)

type Player struct {
	Name               string   `json:"name"`
	Hand               []string `json:"hand"`
	Answer             string   `json:"answer"`
	OriginalAssumption string   `json:"original_assumption"`
	Score              int      `json:"score"`
	Old_Score          int      `json:"old_score"`
	Lost               bool     `json:"-"`
}

const OpenedState = "opened"

type OpenedStateData struct {
	State   string                `json:"state"`
	Code    string                `json:"code"`
	Players map[uuid.UUID]*Player `json:"players"`
}

const ChoosingState = "choosing"

type ChoosingStateData struct {
	State   string                `json:"state"`
	Chooser uuid.UUID             `json:"chooser"`
	Players map[uuid.UUID]*Player `json:"players"`
	Queue   []uuid.UUID           `json:"-"`
}

const AnsweringState = "answering"

type AnsweringStateData struct {
	State       string                `json:"state"`
	Chooser     uuid.UUID             `json:"chooser"`
	Players     map[uuid.UUID]*Player `json:"players"`
	Description string                `json:"description"`
	KeyImg      string                `json:"-"`
	Queue       []uuid.UUID           `json:"-"`
}

const SearchOriginalState = "search_original"

type SearchOriginalStateData struct {
	State       string                `json:"state"`
	Chooser     uuid.UUID             `json:"chooser"`
	Players     map[uuid.UUID]*Player `json:"players"`
	Description string                `json:"description"`
	Queue       []uuid.UUID           `json:"-"`
}

const ShowOriginalState = "show_original"

type ShowOriginalStateData struct {
	State       string                `json:"state"`
	Chooser     uuid.UUID             `json:"chooser"`
	Players     map[uuid.UUID]*Player `json:"players"`
	Description string                `json:"description"`
	Queue       []uuid.UUID           `json:"-"`
}

const ShowScoreState = "show_score"

type ShowScoreStateData struct {
	State   string                `json:"state"`
	Players map[uuid.UUID]*Player `json:"players"`
	Queue   []uuid.UUID           `json:"-"`
}

const ShowWinnerState = "show_winner"

type ShowWinnerStateData struct {
	State   string                `json:"state"`
	Players map[uuid.UUID]*Player `json:"players"`
	Winner  string                `json:"winner"`
}

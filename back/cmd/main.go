package main

import (
	"flag"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/Ivanchgeek/memarium/back/manager"
)

var (
	mng *manager.Manager
)

func init() {
	mng = manager.NewManager()
	go mng.Run()
}

func main() {
	port := flag.String("port", "8000", "serving port")
	flag.Parse()

	r := mux.NewRouter()
	r.HandleFunc("/api/createLobby",
		func(w http.ResponseWriter, r *http.Request) {
			name := r.FormValue("name")
			mng.CreateLobby(name, w, r)
		},
	)
	r.HandleFunc("/api/joinLobby/{code}",
		func(w http.ResponseWriter, r *http.Request) {
			code := mux.Vars(r)["code"]
			name := r.FormValue("name")
			mng.ConnectToLobby(code, name, w, r)
		},
	)
	r.HandleFunc("/api/joinRequestCheck/{code}",
		func(w http.ResponseWriter, r *http.Request) {
			code := mux.Vars(r)["code"]
			name := r.FormValue("name")
			mng.ConnectRequestCheck(code, name, w, r)
		},
	)
	r.HandleFunc("/api/reconnectLobby/{key}",
		func(w http.ResponseWriter, r *http.Request) {
			key := mux.Vars(r)["key"]
			mng.ReconnectToLobby(key, w, r)
		},
	)
	r.PathPrefix("/memes/").Handler(http.FileServer(http.Dir("./")))

	if err := http.ListenAndServe(":"+*port, cors.Default().Handler(r)); err != nil {
		panic(err)
	}
}

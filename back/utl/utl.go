package utl

func ContainsCard(s []string, e string) bool {
	for _, el := range s {
		if el == e {
			return true
		}
	}
	return false
}

func ReplaceCard(s []string, e string, upd string) []string {
	result := make([]string, len(s))
	for i, el := range s {
		if el == e {
			result[i] = upd
		} else {
			result[i] = el
		}
	}
	return result
}

package manager

import (
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

const (
	writeWait      = 10 * time.Second
	pongWait       = 20 * time.Second
	pingPeriod     = (pongWait * 9) / 10
	maxMessageSize = 512
)

type ConnectionMessage struct {
	Sender uuid.UUID
	Data   []byte
}

type Connection struct {
	Id       uuid.UUID
	Send     chan []byte
	deattach chan uuid.UUID
	message  chan ConnectionMessage
	conn     *websocket.Conn
}

func newConnection(message chan ConnectionMessage, deattach chan uuid.UUID, conn *websocket.Conn) *Connection {
	return &Connection{
		Id:       uuid.New(),
		Send:     make(chan []byte),
		message:  message,
		deattach: deattach,
		conn:     conn,
	}
}

func (p *Connection) readPump() {
	defer func() {
		p.deattach <- p.Id
		p.conn.Close()
	}()
	p.conn.SetReadDeadline(time.Now().Add(pongWait))
	p.conn.SetPongHandler(func(string) error { p.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		msgType, data, err := p.conn.ReadMessage()
		if err != nil {
			if strings.Contains(err.Error(), "close") {
				return
			}
			break
		}
		if msgType != websocket.PingMessage && msgType != websocket.PongMessage {
			p.message <- ConnectionMessage{
				Sender: p.Id,
				Data:   data,
			}
		}
	}
}

func (p *Connection) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		p.conn.Close()
	}()
	for {
		select {
		case message, ok := <-p.Send:
			p.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				p.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			w, err := p.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)
			n := len(p.Send)
			for i := 0; i < n; i++ {
				w.Write(<-p.Send)
			}
			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			p.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := p.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

package manager

import (
	"net/http"
	"strings"

	"github.com/google/uuid"
)

const (
	charSet     = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	initialCode = "ZZZZ"
)

type Manager struct {
	lobbies  map[string]*Lobby
	deattach chan string
	lastCode string
}

func NewManager() *Manager {
	return &Manager{
		lobbies:  make(map[string]*Lobby),
		deattach: make(chan string),
		lastCode: initialCode,
	}
}

func (m *Manager) Run() {
	for {
		id := <-m.deattach
		delete(m.lobbies, id)
	}
}

func (m *Manager) CreateLobby(name string, w http.ResponseWriter, r *http.Request) {
	id := m.getCode()
	subject := newLobby(id, m.deattach)
	if err := subject.addPlayer(name, w, r); err != nil {
		return
	}
	m.lobbies[id] = subject
	go subject.run()
}

func (m *Manager) ConnectToLobby(id string, name string, w http.ResponseWriter, r *http.Request) {
	id = strings.ToUpper(id)
	if m.lobbies[id] != nil {
		m.lobbies[id].addPlayer(name, w, r)
	} else {
		http.Error(w, "no such lobby", http.StatusBadRequest)
	}
}

func (m *Manager) ConnectRequestCheck(id string, name string, w http.ResponseWriter, r *http.Request) {
	id = strings.ToUpper(id)
	w.WriteHeader(http.StatusOK)
	if m.lobbies[id] != nil {
		if _, err := m.lobbies[id].game.IsAddingAllowed(name); err != nil {
			w.Write([]byte(err.Error()))
		} else {
			w.Write([]byte("ok"))
		}
	} else {
		w.Write([]byte("no such lobby"))
	}
}

func (m *Manager) ReconnectToLobby(reconnectKey string, w http.ResponseWriter, r *http.Request) {
	data := strings.Split(reconnectKey, "&")
	if len(data) != 2 {
		return
	}
	id, err := uuid.FromBytes([]byte(data[1]))
	if err != nil {
		return
	}
	if m.lobbies[data[0]] != nil {
		m.lobbies[data[0]].reconnectPlayer(id, w, r)
	}
}

func (m *Manager) getCode() string {
	chars := []rune(m.lastCode)
	for i, char := range chars {
		pos := strings.IndexRune(charSet, char) + 1
		if pos < len(charSet) {
			chars[i] = ([]rune(charSet))[pos]
			break
		} else {
			chars[i] = ([]rune(charSet))[0]
		}
	}
	m.lastCode = string(chars)
	return m.reverseCode(chars)
}

func (m *Manager) reverseCode(runes []rune) string {
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

package manager

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"gitlab.com/Ivanchgeek/memarium/back/game"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  4096,
	WriteBufferSize: 4096,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Lobby struct {
	Code               string
	Players            map[uuid.UUID]*Connection
	game               *game.Game
	connectionMessages chan ConnectionMessage
	connectionDeattach chan uuid.UUID
	managerDeattach    chan string
	gameMessages       chan game.GameMessage
}

func newLobby(id string, managerDeattach chan string) *Lobby {
	log.Printf("New lobby created (id: %s)", id)
	l := &Lobby{
		Code:               id,
		Players:            make(map[uuid.UUID]*Connection),
		gameMessages:       make(chan game.GameMessage, game.Config.Values.MaxPlayers),
		connectionMessages: make(chan ConnectionMessage, game.Config.Values.MaxPlayers*3),
		connectionDeattach: make(chan uuid.UUID, game.Config.Values.MaxPlayers),
		managerDeattach:    managerDeattach,
	}
	l.game = game.NewGame(id, l.gameMessages)
	return l
}

func (l *Lobby) run() {
	for {
		select {
		case msg := <-l.connectionMessages:
			l.proccessCommand(msg)
		case msg := <-l.gameMessages:
			l.sendMessage(msg)
		case id := <-l.connectionDeattach:
			l.game.DisconnectPlayer(id)
			l.Players[id].conn.Close()
			delete(l.Players, id)
			log.Printf("Player disconnected (Lobby code: %s)", l.Code)
			if len(l.Players) == 0 {
				l.close()
				return
			}
		}
	}
}

func (l *Lobby) addPlayer(name string, w http.ResponseWriter, r *http.Request) error {
	if allowed, err := l.game.IsAddingAllowed(name); !allowed {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return err
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		conn.Close()
		return err
	}
	subject := newConnection(l.connectionMessages, l.connectionDeattach, conn)
	l.Players[subject.Id] = subject
	go subject.readPump()
	go subject.writePump()
	l.game.AddPlayer(subject.Id, name, l.Code)
	log.Printf("New player added (Lobby code: %s)", l.Code)
	return nil
}

func (l *Lobby) reconnectPlayer(id uuid.UUID, w http.ResponseWriter, r *http.Request) {
	if !l.game.IsReconnectionAllowed(id) {
		return
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		conn.Close()
		return
	}
	subject := newConnection(l.connectionMessages, l.connectionDeattach, conn)
	l.Players[subject.Id] = subject
	go subject.readPump()
	go subject.writePump()
	l.game.ReconnectPlayer(id)
	log.Printf("Player reconnected (Lobby code: %s)", l.Code)
}

func (l *Lobby) sendMessage(msg game.GameMessage) {
	loadage, err := json.Marshal(msg)
	if err != nil {
		return
	}
	l.Players[msg.Target].Send <- loadage
}

func (l *Lobby) proccessCommand(msg ConnectionMessage) {
	var cmd map[string]interface{}
	if err := json.Unmarshal(msg.Data, &cmd); err != nil {
		return
	}
	l.game.ProccessCommand(msg.Sender, cmd)
}

func (l *Lobby) close() {
	for _, player := range l.Players {
		player.conn.Close()
	}
	close(l.connectionMessages)
	close(l.connectionDeattach)
	l.managerDeattach <- l.Code
	log.Printf("Lobby closed (id: %s)", l.Code)
}

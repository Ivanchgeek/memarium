jest.mock('./src/mixins/lang', () => {
    return {
      computed: {
        lang() {
          return new Proxy({}, {
            get(t, p) {
              return 'test lang'
            }
          })
        }
      }
    }
})

const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '^/api/': {
        target: 'http://0.0.0.0:8000',
        ws: true,
        changeOrigin: true
      },
      '^/memes/': {
        target: 'http://0.0.0.0:8000'
      }
    }
  }
})

import { shallowMount } from "@vue/test-utils"
import MenuView from '@/views/MenuView'
import RegularContainer from '@/components/RegularContainer'

describe('MenuView', () => {

    it('deactivates', async () => {
        const wrapper = shallowMount(MenuView)
        await wrapper.setData({viewActive: false})
        const actives = wrapper.findAllComponents(RegularContainer)
            .wrappers.map(wrp => wrp.props('active'))
        expect(actives).not.toContain(true)
    })

})
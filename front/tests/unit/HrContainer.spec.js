import { mount } from "@vue/test-utils"
import HrContainer from '@/components/HrContainer'

describe('HrContainer', () => {

    it('displays slot', () => {
        const wrapper = mount(HrContainer, {
            slots: {
                default: '<div class="test">sloted</div>'
            }
        })
        expect(wrapper.find('.test').text()).toBe('sloted')
    })

    it('implements top aligment', () => {
        const wrapper = mount(HrContainer, {
            propsData: {
                top: '10%'
            }
        })
        expect(wrapper.find('.container').element.style.top).toBe('10%')
    })

    it('implements bottom aligment', () => {
        const wrapper = mount(HrContainer, {
            propsData: {
                bottom: '10%'
            }
        })
        expect(wrapper.find('.container').element.style.bottom).toBe('10%')
    })

})
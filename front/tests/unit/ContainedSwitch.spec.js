import { mount } from "@vue/test-utils"
import ContainedSwitch from '@/components/ContainedSwitch'

describe('RegularContainer', () => {

    it('implements check preset', async () => {
        const wrapper = mount(ContainedSwitch, {
            propsData: {
                checked: false
            }
        })
        expect(wrapper.find('input').element.checked).toBe(false)
        await wrapper.setProps({checked: true})
        expect(wrapper.find('input').element.checked).toBe(true)
    })

    it('triggers event', async () => {
        const wrapper = mount(ContainedSwitch)
        await wrapper.find('input').setChecked(true)
        expect(wrapper.emitted()).toEqual({change: [[true]]})
    })

})
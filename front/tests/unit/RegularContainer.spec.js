import { mount } from "@vue/test-utils"
import RegularContainer from '@/components/RegularContainer'

describe('RegularContainer', () => {

    it('displays slot', () => {
        const wrapper = mount(RegularContainer, {
            slots: {
                default: '<div class="test">sloted</div>'
            }
        })
        expect(wrapper.find('.test').text()).toBe('sloted')
    })

    it('clickable', async () => {
        const wrapper = mount(RegularContainer, {
            propsData: {
                button: true
            }
        })
        await wrapper.find('div').trigger('click')
        expect(wrapper.emitted().click.length).toBe(1)
    })

    it('implements left aligment', () => {
        const wrapper = mount(RegularContainer)
        expect(wrapper.find('.container').classes()).toContain('left')
        expect(wrapper.find('.container').classes()).not.toContain('right')
    })

    it('implements right aligment', () => {
        const wrapper = mount(RegularContainer, {
            propsData: {
                right: true
            }
        })
        expect(wrapper.find('.container').classes()).toContain('right')
        expect(wrapper.find('.container').classes()).not.toContain('left')
    })

    it('implements top aligment', () => {
        const wrapper = mount(RegularContainer, {
            propsData: {
                top: '10%'
            }
        })
        expect(wrapper.find('.container').element.style.top).toBe('10%')
    })

    it('implements bottom aligment', () => {
        const wrapper = mount(RegularContainer, {
            propsData: {
                bottom: '10%'
            }
        })
        expect(wrapper.find('.container').element.style.bottom).toBe('10%')
    })

    it('deactivates', () => {
        const wrapper = mount(RegularContainer, {
            propsData: {
                active: false
            }
        })
        expect(wrapper.find('.container').exists()).toBe(false)
    })

})
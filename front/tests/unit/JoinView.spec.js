import { shallowMount } from "@vue/test-utils"
import JoinView from '@/views/JoinView'
import RegularContainer from '@/components/RegularContainer'

describe('AboutView', () => {

    it('deactivates', async () => {
        const wrapper = shallowMount(JoinView)
        await wrapper.setData({viewActive: false})
        const actives = wrapper.findAllComponents(RegularContainer)
            .wrappers.map(wrp => wrp.props('active'))
        expect(actives).not.toContain(true)
    })

})
import { mount } from "@vue/test-utils"
import BackArrow from '@/components/BackArrow'

describe('BackArrow', () => {

    it('implements top alligment', () => {
        const wrapper = mount(BackArrow, {
            propsData: {
                top: "10%"
            }
        })
        expect(wrapper.find('.container').element.style.top).toBe('10%')
    })

    it('deactivates', () => {
        const wrapper = mount(BackArrow, {
            propsData: {
                active: false
            },
            computed: {
                backFreezed: () => false
            }
        })
        expect(wrapper.find('.container').exists()).toBe(false)
    })

    it('freezes', () => {
        const wrapper = mount(BackArrow, {
            propsData: {
                active: false
            },
            computed: {
                backFreezed: () => true
            }
        })
        expect(wrapper.find('.container').exists()).toBe(true)
    })

    it('emits hide request', async () => {
        const wrapper = mount(BackArrow)
        await wrapper.find('.container').trigger('click')
        expect(wrapper.emitted()).toEqual({'hide-request': [[]]})
    })

})
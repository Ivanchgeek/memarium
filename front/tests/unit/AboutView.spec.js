import { shallowMount } from "@vue/test-utils"
import AboutView from '@/views/AboutView'
import RegularContainer from '@/components/RegularContainer'

describe('AboutView', () => {

    it('deactivates', async () => {
        const wrapper = shallowMount(AboutView)
        await wrapper.setData({viewActive: false})
        const actives = wrapper.findAllComponents(RegularContainer)
            .wrappers.map(wrp => wrp.props('active'))
        expect(actives).not.toContain(true)
    })

})
import { mount } from "@vue/test-utils"
import ContainedCodeInput from '@/components/ContainedCodeInput'
import Vue from "vue"

describe('ContainedCodeInput', () => {

    it('linking props', async () => {
        const wrapper = mount(ContainedCodeInput)
        await wrapper.setProps({value: 'AAAA'})
        expect(wrapper.find('input').element.value).toBe('A-A-A-A')
    })

    it('calls change on input and input correct chars in correct way', () => {
        const wrapper = mount(ContainedCodeInput)
        wrapper.find('input').element.dispatchEvent(new KeyboardEvent('input', {inputType: 'insertText', data: 'a'}))
        expect(wrapper.emitted().input).toBeTruthy()
    })

})
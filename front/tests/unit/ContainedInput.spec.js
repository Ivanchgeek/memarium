import { mount } from "@vue/test-utils"
import ContainedInput from '@/components/ContainedInput'

describe('ContainedInput', () => {

    it ('activates (label)', () => {
        const wrapper = mount(ContainedInput, {
            propsData: {
                label: 'test',
                active: true
            }
        })
        expect(wrapper.find('span').exists()).toBe(true)
        expect(wrapper.find('span').text()).toBe('test')
    })

    it ('deactivates (label)', () => {
        const wrapper = mount(ContainedInput, {
            propsData: {
                label: 'test',
                active: false
            }
        })
        expect(wrapper.find('span').exists()).toBe(false)
    })

    it('implements value preset', () => {
        const wrapper = mount(ContainedInput, {
            propsData: {
                value: 'test'
            }
        })
        expect(wrapper.find('input').element.value).toBe('test')
    })

    it('triggers event', async () => {
        const wrapper = mount(ContainedInput)
        await wrapper.find('input').setValue('test')
        expect(wrapper.emitted()).toEqual({input: [['test']]})
    })

    it('implements placeholder', () => {
        const wrapper = mount(ContainedInput, {
            propsData: {
                placeholder: 'test'
            }
        })
        expect(wrapper.find('input').element.placeholder).toBe('test')
    })

})
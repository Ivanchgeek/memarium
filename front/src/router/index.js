import Vue from 'vue'
import VueRouter from 'vue-router'
import vals from '@/assets/vals.json'
import MenuView from '@/views/MenuView'
import CreateView from '@/views/CreateView'
import WaitingRoomView from '@/views/WaitingRoomView'
import JoinView from '@/views/JoinView'
import GameView from '@/views/GameView'
import ShowWinnerView from '@/views/ShowWinnerView'

Vue.use(VueRouter)

const routes = [
  {
    path: vals.routes.menu,
    name: 'menu',
    component: MenuView
  },
  {
    path: vals.routes.about,
    name: 'about',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: vals.routes.create,
    name: 'create',
    component: CreateView
  },
  {
    path: vals.routes.waitingRoom,
    name: 'waitingRoom',
    component: WaitingRoomView
  },
  {
    path: vals.routes.join,
    name: 'join',
    component: JoinView
  },
  {
    path: vals.routes.game,
    name: 'game',
    component: GameView
  },
  {
    path: vals.routes.showWinner,
    name: 'showWinner',
    component: ShowWinnerView
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

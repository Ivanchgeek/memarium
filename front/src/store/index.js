import Vue from 'vue'
import Vuex from 'vuex'
import settings from './modules/settings'
import content from './modules/content'
import game from './modules/game'
import notifier from './modules/notifier'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    settings,
    content,
    game,
    notifier
  }
})
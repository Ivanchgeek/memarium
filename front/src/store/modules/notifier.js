const state = () => ({
    notifications: []
})

const mutations = {
    push (state, msg) {
        let update = state.notifications
        update.push(msg)
        state.notifications = update
    },
    pop (state) {
        let update = state.notifications
        update.splice(0, 1)
        state.notifications = update
    }
}

const actions = {
    notify : {
        root: true,
        handler ({commit}, msg) {
            commit('push', msg)
        }
    }
}

export default {
    namespaced: true,
    state, mutations, actions
}
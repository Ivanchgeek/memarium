import lang from '@/assets/lang'

const state = () => ({
    language: 'ru'
})

const getters = {
    wordData (state) {
        return lang[state.language]
    }
}

const mutations = {
    setLanguage (state, { message }) {
        state.language = message
    }
}

export default {
    namespaced: true,
    state, getters, mutations
}
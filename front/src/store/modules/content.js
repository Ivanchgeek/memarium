const state = () => ({
    backFreezed: false
})

const mutations = {
    freezeBack (state) {
        state.backFreezed = true
    },
    unFreezeBack (state) {
        state.backFreezed = false
    },
}

export default {
    namespaced: true,
    state, mutations
}
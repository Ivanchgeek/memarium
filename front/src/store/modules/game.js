import * as api from '@/api/api'

const state = () => ({
    socket: undefined,
    gameState: {},
    iAmIniter: false,
    myId: "",
    config: {}
})

const mutations = {
    setSocket (state, socket) {
        state.socket = socket
    },
    setGameState (state, update) {
        state.gameState = update
    },
    setIAmIniter (state, val) {
       state.iAmIniter = val
    },
    setMyId (state, val) {
        state.myId = val
    },
    setConfig (state, val) {
        state.config = val
    }
}

const getters = {
    code (state) {
        if (state.gameState.state !== state.config.states.opened) {
            return ""
        }
        return state.gameState.code.split('').join('-')
    },
    playersCount (state) {
        return Object.keys(state.gameState.players).length
    },
    me (state) {
        if (state.gameState.state === state.config.states.opened) {
            return {}
        }
        return state.gameState.players[state.myId]
    },
    myHand (state, getters) {
        if (state.gameState.state === state.config.states.opened) {
            return []
        }
        return getters.me.hand
    },
    playersLeftToAnswerCount (state) {
        if (state.gameState.state !== state.config.states.answering) {
            return 0
        }
        return Object.entries(state.gameState.players).filter(
            ([id, pl]) => (id !== state.gameState.chooser && pl.answer === '')
        ).length
    },
    answers (state) {
        if (state.gameState.state !== state.config.states.search_original) {
            return []
        }

        function shuffleArray(array) {
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [array[i], array[j]] = [array[j], array[i]];
            }
            return array
        }

        return shuffleArray(Object.entries(state.gameState.players).map(
            ([id, pl]) => ({
                meme: pl.answer,
                my: id === state.myId
            })
        ))
    }
}

const actions = {
    createLobby (contex, data) {
        api.CreateLobby(contex, data)
    },
    joinLobby (contex, data) {
        api.JoinLobby(contex, data)
    },
    exit ({ state, commit }) {
        if (state.socket) {
            state.socket.close() 
            commit('setSocket', undefined)
            commit('setIAmIniter', false)
            commit('setMyId', "")
        }
    },
    send({ state }, message) {
        console.log('output msg: ', message)
        state.socket.send(message)
    }
}

export default {
    namespaced: true,
    state, getters, mutations, actions
}
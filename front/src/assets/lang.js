const ru = require('./translations/ru.json')

export const languages = ['ru']

const data = {
    ru
}

const handler = {
    get: function(target, name) {
        return Object.prototype.hasOwnProperty.call(target, name) ? target[name] : ru;
    }
};

export default new Proxy(data, handler)
export default {
    computed : {
      backFreezed () {
        return this.$store.state.content.backFreezed
      }
    },
    methods: {
        freezeBack () {
            this.$store.commit('content/freezeBack')
        },
        unFreezeBack () {
            this.$store.commit('content/unFreezeBack')
        }
    },
}
export default {
    computed : {
      lang () {
        return this.$store.getters['settings/wordData']
      }
    }
}

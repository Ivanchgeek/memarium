export const apiURL = ''
export const wsApiURL = window.location.origin.replace("http", "ws")

export const winScore = 40

export function CreateLobby({ commit, state, dispatch }, { name }) {
    if (state.socket) return
    let socket = new WebSocket(`${wsApiURL}/api/createLobby?name=${name}`)
    setSocketHandlers(socket, state, commit, dispatch)
    commit('setSocket', socket)
    commit('setIAmIniter', true)
}

export async function JoinPermited(name, code) {
    return await fetch(`${apiURL}/api/joinRequestCheck/${code}?name=${name}`).then(r => {
        return r.text()
    })
}

export function JoinLobby({ commit, state, dispatch }, { code, name }) {
    if (state.socket) return
    let socket = new WebSocket(`${wsApiURL}/api/joinLobby/${code}?name=${name}`)
    setSocketHandlers(socket, state, commit, dispatch)
    commit('setSocket', socket)
    commit('setIAmIniter', false)
}

function ReconnectLobby(commit, state, dispatch) {
    let key = window.localStorage.getItem('reconnect_key')
    let socket = new WebSocket(`${wsApiURL}/api/reconnectLobby/${key}`)
    setSocketHandlers(socket, state, commit, dispatch)
    commit('setSocket', socket)
}

function setSocketHandlers(socket, state, commit, dispatch) {
    socket.onclose = event => {
        console.log('connection closed')
        if (event.code !== 1005) {
            console.error(event)
            ReconnectLobby(commit, state, dispatch)
            return 
        }
        commit('setConfig', {})
        commit('setSocket', undefined)
        commit('setMyId', "")
    }
    socket.onmessage = msg => {
        try {
            let json = JSON.parse(msg.data)
            processMessage(json, state, commit, dispatch)
        } catch (e) {
            console.error(e)
            return
        }
    }
    socket.onerror = err => {
        console.error(err)
    }
}

function processMessage(msg, state, commit, dispatch) {
    if (!state.config.message_types) state.config.message_types = {init: 'init'}
    console.log(msg)
    switch (msg.type) {
        case state.config.message_types.error:
            errorHandler(msg.data, dispatch)
            break
        case state.config.message_types.state_update:
            commit('setGameState', msg.data)
            break
        case state.config.message_types.init:
            commit('setMyId', msg.data.id)
            commit('setConfig', msg.data.config)
            window.localStorage.setItem('reconnect_key', msg.data.reconnect_key)
            break
    }
}

function errorHandler(msg, dispatch) {
    switch (msg) {
        case 'need more players':
            dispatch('notify', 'слишком мало игроков', { root: true })
            break
        case 'too much players':
            dispatch('notify', 'слишком много игроков', { root: true })
            break
        default:
            alert('error: ' + msg)
    }
}
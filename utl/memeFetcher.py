import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
import urllib
import os
import re
import memeFetcherConf

def write_msg(user_id, message):
    vk.method('messages.send', {'user_id': user_id, 'message': message, 'random_id': 123123})

vk = vk_api.VkApi(token=memeFetcherConf.token)
longpoll = VkLongPoll(vk)

loaded = 0
files = []

if not os.path.isdir(memeFetcherConf.folder):
    os.makedirs(memeFetcherConf.folder)

legacy = [f for f in os.listdir(memeFetcherConf.folder) if os.path.isfile(os.path.join(memeFetcherConf.folder, f))]

for file in legacy:
    num = int(re.search('meme(.+?).jpg', file).group(1))
    if num >= loaded:
        loaded = num + 1

for event in longpoll.listen():
    if event.type == VkEventType.MESSAGE_NEW:
        if event.to_me:

            if (event.text == 'dump'):
                with open(memeFetcherConf.output, "w") as f:
                    f.write('let images = ["{}"]'.format('","'.join(files)))
                    f.close()
                exit()

            msg = vk.get_api().messages.get_by_id(message_ids=event.message_id)
            for atach in msg['items'][0]['attachments']:
                if (loaded >= memeFetcherConf.count):
                    write_msg(event.user_id, 'тормози')
                    break
                try:
                    name = memeFetcherConf.folder + 'meme{}.jpg'.format(loaded)
                    urllib.request.urlretrieve(atach['photo']['sizes'][-1]['url'], memeFetcherConf.folder + 'meme{}.jpg'.format(loaded))
                    loaded += 1
                    files.append(name)
                    print(name, ' loaded')
                except:
                    write_msg(event.user_id, 'картиночка потерялась')
                    print('some error occured')
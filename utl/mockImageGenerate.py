from PIL import Image, ImageDraw, ImageFont

for i in range(200):
    img = Image.new('RGB', (600, 600), color = (255, 255, 255))
 
    fnt = ImageFont.truetype('/usr/share/fonts/CeraPro-Regular.ttf', 50)

    d = ImageDraw.Draw(img)
    d.text((215,270), "Meme {}".format(i), fill=(88, 135, 255), font=fnt)
    print(i)
    
    img.save("./back/memes/meme{}.jpg".format(i))
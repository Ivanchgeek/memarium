const outputSize = 600
const minCropSize = 100

let img
let source, frame
let fx = fy = 0
let fsize = 100

function init() {
    source = document.getElementById('source')
    frame = document.querySelector('.frame')

    source.addEventListener('mousemove', moveFrame)
    source.addEventListener('mousewheel', scaleFrame)
    window.addEventListener('keyup', saveCropped)
    nextImage()
}

function nextImage() {
    console.log(images.length)
    fsize = minCropSize
    frame.style.width = fsize + 'px'
    frame.style.height = fsize + 'px'
    img = images.shift()
    if (!img) {
        alert('that\'s all')
        return
    }
    let image = new Image()
    image.src = img
    image.onload = () => {
        source.width = image.width
        source.height = image.height
        source.getContext('2d').drawImage(image, 0, 0)
    }
}

function moveFrame(event) {
    let updatex = event.pageX
    let updatey = event.pageY
    if (updatex + fsize > source.width) updatex = fx
    if (updatey + fsize > source.height) updatey = fy
    fx = updatex
    fy = updatey

    frame.style.left = fx + 'px'
    frame.style.top = fy + 'px'
}

function scaleFrame(event) {
    let update = fsize + event.deltaY
    if (update < minCropSize) return
    if (fx + update > source.width) return
    if (fy + update > source.height) return
    fsize = update

    frame.style.width = fsize + 'px'
    frame.style.height = fsize + 'px'
}

function saveCropped(event) {
    if (event.key !== 'Enter') return

    source.width = outputSize
    source.height = outputSize

    let image = new Image()
    image.setAttribute('crossorigin', 'anonymous')
    image.src = img
    image.onload = () => {
        source.getContext('2d').drawImage(image, 
            fx, fy, fsize, fsize,
            0, 0, outputSize, outputSize)
        let crop = source.toDataURL("image/png").replace("image/png", "image/octet-stream")
        let link = document.createElement('a')
        link.href = crop
        link.download = img.match(/meme*.*$/)[0]
        document.documentElement.appendChild(link)
        new Promise(() => {
            link.click()
            nextImage()
        })
    }
}

window.addEventListener('load', init)